:- module(screen, [clear/0]).

% clear the screen
clear :- put_code(27), write('[2J'), ttyflush.
