:- module(_,[main/1]).

:- use_module(screen).

% Say hello!
main(_) :-
	clear,
	write('Welcome to City Kids'), nl,
	write('A game by Skye Bleed'), nl,
	write('(press enter to begin) '),
	flush_output,
	get_code(_),

	start_game.

start_game :-
	clear,
	write('started'), nl,
	loop.

loop :- 
	write(' '),
	flush_output,
	get_char(C),
	test_char(C).

test_char('q') :- end.
test_char(_) :- loop.

end :- write('Done!'), nl.
