build = ./build

some = ${build}

all: ${some} ${build}/city-kids

${build}:
	mkdir -p ${build}

${build}/city-kids: src/main.pl ${some}
	ciaoc -o ${build}/city-kids src/main.pl

# phony targets
run: all
	${build}/city-kids

clean:
	rm -rf build src/*.itf src/*.po*
